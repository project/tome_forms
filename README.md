# Tome Forms

This module allows Drupal forms to be exported a Tome static site, with the form
submission handled in a variety of ways.

For example, the form data can be sent in an email, whose recipient can be
set in configuration.

This module is inspired by the [Static Site Contact Form
module](https://www.drupal.org/project/static_site_contact_form).

## Requirements

This module requires the following modules:

- [tome](https://www.drupal.org/project/tome)

For Tome form handlers that use a local script, the server hosting the Tome
static site must have PHP available.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to Administration › Configuration › Tome Static › Tome forms.
2. Optionally, under the 'Security handlers' tab, create one or more security
   handlers. These can be applied to forms to help prevent DDOS and other
   attacks.
3. Under the 'Tome forms' tab, click 'Add Tome form' to create a new Tome form
   configuration.
4. Enter the form ID of a form to use as a Tome static form.
5. Optionally, enter paths to export in Tome. If the path(s) on which your form
   is output are already exported by Tome, leave this empty.
6. Select a form handler to use for this form. This determines what happens when
   the static version of the form is submitted.
7. Select one or more of the security handlers that were created earlier.
8. Optionally, you can set paths for the form to send the user on submission,
   whether that is a success or a validation failure.

## Usage

Deploy your Tome static site as normal. You can set form paths to add to the
export in the Tome form config entity, or take care of exporting them yourself.

## Developers

Each Tome static form is represented by a Tome form config entity. This uses a
Tome form handler plugin to create a PHP script which will handle the form
submission on the static site.

The form handler plugins can be local or remote. For each form that uses a local
handler plugin, the Tome export process writes a PHP script file into the
/tome-form-handler folder.

The script verifies the incoming form ID and performs validation for all the
security handlers which the form is configured to use. If validation is
successful, the script handles the form data according to the particular form
handler plugin.

Forms in the HTML output which are the target of a Tome form config entity are
altered so that their form action points to the corresponding script.

You can write a form handler plugin which submits the form to a remote server.
If is then YOUR responsibility to have that remote server's script validate the
form. Some security handler plugins can't be used with a remote script, such as
the Form Token plugin.

## Roadmap

- Add a form handler plugin to post to a REST endpoint?
- Consider adding a setting to output the form ID on every form, to help
  non-devs set up forms.
