<?php

namespace Drupal\tome_forms\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\tome_forms\Entity\TomeFormInterface;
use Drupal\tome_static\StaticGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the form handler PHP script.
 *
 * This outputs raw PHP code for the Tome export process to write to a PHP file.
 *
 * @see \Drupal\tome_forms\EventSubscriber\StaticFormSubscriber
 */
class TomeFormHandlerExportController {

  /**
   * Callback for the tome form script handler route.
   */
  public function content(TomeFormInterface $tome_form, Request $request) {
    $php = $tome_form->getFormHandlerScriptPhp();
    $response = new Response($php);

    return $response;
  }

  /**
   * Checks access for the tome form script handler route.
   *
   * This only allows access to the PHP script during a Tome export.
   *
   * For development, it can be useful to make this allow unconditional access
   * to check the contents of the PHP script without performing an export.
   *
   * @todo Add the request as a parameter when 10.2 is the minimum version of
   * Drupal core.
   */
  public function access() {
    $request = \Drupal::request();
    return AccessResult::allowedIf($request->attributes->get(StaticGeneratorInterface::REQUEST_KEY) == StaticGeneratorInterface::REQUEST_KEY);
  }

}
