<?php

namespace Drupal\tome_forms\Plugin\TomeFormSecurityHandler;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tome_forms\Entity\TomeFormInterface;
use Drupal\tome_forms\Entity\TomeFormSecurityInterface;

/**
 * Interface for Tome Form Security plugins.
 */
interface TomeFormSecurityHandlerInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Alters a Tome form to add security handling.
   *
   * This should add to the form anything that is required for verification by
   * the static export PHP script.
   *
   * @param array $form
   *   The form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\tome_forms\Entity\TomeFormInterface $tome_form
   *   The Tome form entity.
   * @param \Drupal\tome_forms\Entity\TomeFormSecurityInterface $tome_form_security
   *   The Tome security handler entity.
   */
  public function formAlter(&$form, FormStateInterface $form_state, TomeFormInterface $tome_form, TomeFormSecurityInterface $tome_form_security): void;

  /**
   * Gets the PHP code to validate this security handler.
   *
   * This code is inserted into the code for the form handler. This code has
   * access to the variables defined in
   * \Drupal\tome_forms\Plugin\TomeFormHandler\TomeFormHandlerBase::getScriptHeader().
   *
   * @param \Drupal\tome_forms\Entity\TomeFormInterface $tome_form
   *   The Tome form entity.
   * @param \Drupal\tome_forms\Entity\TomeFormSecurityInterface $tome_form_security
   *   The Tome security handler entity.
   *
   * @return array
   *   An array of PHP code lines.
   *
   * @see \Drupal\tome_forms\Plugin\TomeFormHandler\TomeFormHandlerBase::getScriptHeader()
   */
  public function getFormHandlerScriptSecurityCheckPhp(TomeFormInterface $tome_form, TomeFormSecurityInterface $tome_form_security): array;

  /**
   * Gets whether this plugin requires a local PHP script for form validation.
   *
   * @return bool
   *   TRUE if the form security handler needs a local script, FALSE if it can
   *   be used with a remote script.
   */
  public function needsLocalScript(): bool;

}
