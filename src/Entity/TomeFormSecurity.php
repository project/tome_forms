<?php

namespace Drupal\tome_forms\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\tome_forms\Plugin\TomeFormSecurityHandler\TomeFormSecurityHandlerInterface;

/**
 * Provides the Tome Form Security entity.
 *
 * @ConfigEntityType(
 *   id = "tome_form_security",
 *   label = @Translation("Tome form security handler"),
 *   label_collection = @Translation("Tome form security handlers"),
 *   label_singular = @Translation("tome form security handler"),
 *   label_plural = @Translation("tome form security handlers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tome form security handler",
 *     plural = "@count tome form security handlers",
 *   ),
 *   handlers = {
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\tome_forms\Form\TomeFormSecurityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\tome_forms\Entity\Handler\TomeFormSecurityListBuilder",
 *   },
 *   admin_permission = "administer tome_form_security entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "security_plugin_id",
 *     "security_plugin_config",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/tome/static/tome_form_security/add",
 *     "canonical" = "/admin/config/tome/static/tome_form_security/{tome_form_security}",
 *     "collection" = "/admin/config/tome/static/tome_form_security",
 *     "edit-form" = "/admin/config/tome/static/tome_form_security/{tome_form_security}/edit",
 *     "delete-form" = "/admin/config/tome/static/tome_form_security/{tome_form_security}/delete",
 *   },
 * )
 */
class TomeFormSecurity extends ConfigEntityBase implements TomeFormSecurityInterface {

  /**
   * The machine name.
   *
   * @var string
   */
  protected $id = '';

  /**
   * The human-readable label.
   *
   * @var string
   */
  protected $label = '';

  /**
   * The form security handler plugin this entity uses.
   *
   * @var string
   */
  protected $security_plugin_id = '';

  /**
   * The plugin configuration for the form security handler plugin.
   *
   * @var array
   */
  protected $security_plugin_config = [];

  /**
   * The plugin collection that holds the security plugin.
   *
   * @var \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function formAlter(&$form, FormStateInterface $form_state, TomeFormInterface $tome_form): void {
    // Hand over to this entity's Tome form security handler plugin.
    $this->getFormSecurityHandlerPlugin()->formAlter($form, $form_state, $tome_form, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormHandlerScriptSecurityCheckPhp(TomeFormInterface $tome_form): array {
    return $this->getFormSecurityHandlerPlugin()->getFormHandlerScriptSecurityCheckPhp($tome_form, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormSecurityHandlerPlugin(): TomeFormSecurityHandlerInterface {
    return $this->getTomeFormSecurityCollection()->get($this->security_plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    $collections = [];
    if ($collection = $this->getTomeFormSecurityCollection()) {
      $collections['security'] = $collection;
    }
    return $collections;
  }

  /**
   * Gets the plugin collection for the form handler plugin.
   */
  protected function getTomeFormSecurityCollection() {
    if (!$this->pluginCollection && $this->security_plugin_id) {
      $plugin_manager = \Drupal::service('plugin.manager.tome_form_security');
      $this->pluginCollection = new DefaultSingleLazyPluginCollection(
        $plugin_manager,
        $this->security_plugin_id,
        $this->security_plugin_config,
      );
    }
    return $this->pluginCollection;
  }

}
