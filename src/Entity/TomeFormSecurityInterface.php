<?php

namespace Drupal\tome_forms\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tome_forms\Plugin\TomeFormSecurityHandler\TomeFormSecurityHandlerInterface;

/**
 * Interface for Tome Security entities.
 */
interface TomeFormSecurityInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Alters the form being rendered for static output to add security features.
   *
   * This hands over to the Tome form security plugin for this entity.
   *
   * @param array $form
   *   The form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param TomeFormInterface $tome_form
   *   The Tome form entity.
   */
  public function formAlter(&$form, FormStateInterface $form_state, TomeFormInterface $tome_form): void;

  /**
   * Gets the PHP code to validate this security handler.
   *
   * @see \Drupal\tome_forms\Plugin\TomeFormSecurityHandler\TomeFormSecurityHandlerInterface::getFormHandlerScriptSecurityCheckPhp()
   */
  public function getFormHandlerScriptSecurityCheckPhp(TomeFormInterface $tome_form): array;

  /**
   * Gets the form security handler plugin for this form security entity.
   *
   * @return \Drupal\tome_forms\Plugin\TomeFormSecurityHandler\TomeFormSecurityHandlerInterface
   */
  public function getFormSecurityHandlerPlugin(): TomeFormSecurityHandlerInterface;

}
